#import library
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense, BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, EarlyStopping
from matplotlib import pyplot as plt
from keras.preprocessing import image
import numpy as np

import cv2

# hyperparameter
# hyperparameter
batch_size = 30
img_height, img_width = 150, 150
model_path = "best_model.h5"
nb_class = 2
epochs = 5

def first_model():
    model = Sequential()
    # build model
    model.add(Conv2D(32, (3, 3), input_shape=(img_height, img_width, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))  # use dropout to reduce overfitting
    model.add(Dense(nb_class))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model

# kiem tra model
test_model = first_model()
print (test_model.summary())

# du doan tren tap test set
def predict(trained_model_path, img_path, img_height, img_width):
    model = load_model(trained_model_path)
    img = image.load_img(img_path, target_size=(img_height, img_width))
    img = image.img_to_array(img)
    img=img/255
    img = np.expand_dims(img, axis=0)
    output = model.predict(img)
    print(output)

def train(train_data_dir, vali_data_dir, test_data_dir):
    model = first_model()
    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')
    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    # this is a generator that will read pictures found in
    # subfolers of train_data_dir, and indefinitely generate
    # batches of augmented image data
    train_generator = train_datagen.flow_from_directory(
        train_data_dir,  # this is the target directory
        target_size=(img_height, img_width),  # all images will be resized to 150x150
        batch_size=batch_size,
        shuffle=True,
        class_mode='categorical')
    class_indices = train_generator.class_indices
    # this is a similar generator, for validation data
    validation_generator = test_datagen.flow_from_directory(
        vali_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='categorical')
    test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='categorical')
    print("labels ",class_indices )

    # save best model
    checkpoint = ModelCheckpoint(model_path, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    # stop learning if validation accuracy doesn't improve after 6 epochs
    early_stop = EarlyStopping(monitor='val_loss', patience=6, verbose=0, mode='auto')
    # training
    hist = model.fit_generator(
        train_generator,
        steps_per_epoch=len(train_generator),
        epochs=epochs,
        validation_data=validation_generator,
        callbacks=[checkpoint, early_stop],
        validation_steps=len(validation_generator),
    )

    # evaluate on the test set
    model.load_weights(model_path)
    test_score = model.evaluate_generator(test_generator, steps=len(test_generator))
    print("test loss and test accuracy  : {} ,µb {}".format(test_score[0], test_score[1]))


if __name__ == "__main__":
    """
    a training ,validation and testing data directory  directory containing one subdirectory per image class, filled with .png or .jpg images
    data/
        train/
            mask/
                .jpg
                .jpg
                ...
            nomask/
                .jpg
                .jpg
                ...
        validation/
            mask/
                .jpg
                .jpg
                ...
            nomask/
                .jpg
                .jpg
                ...
        test/
            mask/
                .jpg
                .jpg
                ...
            nomask/
                .jpg
                .jpg
                ...
    """
    train_data_dir = "./maskornomask/train"
    vali_data_dir = "./maskornomask/validation"
    test_data_dir = "./maskornomask/test"
    train(train_data_dir=train_data_dir, vali_data_dir=vali_data_dir, test_data_dir=test_data_dir)

